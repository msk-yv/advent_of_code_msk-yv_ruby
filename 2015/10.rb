# target 1113122113
def get_repeat_length(s, c)
  i=1
  while s[i] == c
    i += 1
  end
  return i
end

def form_string(inp)
  answ =''
  i = 0
  while i < inp.size
    c = inp[i]
    j = i + 1
    while inp[j] == c
      j += 1
    end
    l = j - i
    answ += l.to_s + c
    i += l
  end
  return answ
end

inp = '1113122113'
50.times do |i|
  s = form_string(inp)
  puts "#{i} step. Length= #{s.size}" #, s=#{s}"
  inp = s
end
puts inp.size