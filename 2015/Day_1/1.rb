def count(inp)
  return inp.count('(') - inp.count(')')
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now

#TODO Part 1
answer1 = count(inp)

p "Part 1 answer: #{answer1}"
p "Time to Part 1 answer: #{Time.now - start} sec"

# part 2

i=0
current_floor = 0
start = Time.now
while i < inp.size && current_floor != -1 do
  if inp[i] == '('
  	current_floor += 1 
  else
  	current_floor -= 1
  end
  i += 1
end
p "Part 2 answer: #{i} getted in #{Time.now - start} sec"
