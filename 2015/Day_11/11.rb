# input for task
inp="hxbxwxba"

def has_triplet?(s)
  s[0..-3].split('').each_with_index do |c, i|
    if c.next == s[i+1] && c.next.next == s[i+2]
      return true
    end
  end
  false
end

def double_count(s)
  count = 0
  'abcdefghjkmnpqrstuvwxyz'.each_char do |c|
    if s.include?(c*2)
      count += 1
    end
  end
  count
end

def find_next_password(s)
  while s < 'zzzzzzzz' do
    s = s.next
    if s.include?('i') || s.include?('o') || s.include?('l')
      next
    end
    if has_triplet?(s) && double_count(s) > 1
      return s
    end
  end
end

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
answer = find_next_password(inp)

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
# answer = 0 #change type if need

#TODO Part 2

answer = find_next_password(answer)

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
