require 'json'

class String
  def numeric?
    Float(self) != nil rescue false
  end
end

def hash_checker(hash, check_red = false)
  res = 0
  hash.each do |key, value|
    next if check_red && hash.values.include?('red')
    res += if value.class == String
             value.numeric? ? value.to_f : 0
           elsif value.class == Hash
             hash_checker(value, check_red)
           elsif value.class == Array
             array_checker(value, check_red)
           elsif value.class == Integer
             value
           end
  end
  res
end

def array_checker(array, check_red = false )
  res = 0
  array.each do |item|
    res += if item.class == String
             item.numeric? ? item.to_f : 0
           elsif item.class ==  Hash
             hash_checker(item, check_red)
           elsif item.class ==  Array
             array_checker(item, check_red)
           elsif item.class == Integer
             item.to_f
           end
  end
  res
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
 #change type if need

#TODO Part 1
answer = hash_checker(JSON.parse(inp))

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

# #TODO Part 2
answer = hash_checker(JSON.parse(inp), true)

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
