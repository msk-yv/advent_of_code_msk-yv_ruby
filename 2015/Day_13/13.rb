# input for task
inp=IO.read('input.txt')

index_hash = {
  Alice: 0,
  Bob: 1,
  Carol: 2,
  David: 3,
  Eric: 4,
  Frank: 5,
  George: 6,
  Mallory: 7
}

names = %i[Alice Bob Carol David Eric Frank George Mallory] #

# part 1
start = Time.now
answer = 0 #change type if need
single_pairs = {Alice: {}, Bob: {}, Carol: {}, David: {}, Eric: {}, Frank: {}, George: {}, Mallory: {} }

#TODO Part 1
inp.each_line do |line|
  a = line.split('.')[0].split(' ')
  if a[2] == 'lose'
    score = 0 - a[3].to_i
  else
    score = 0 + a[3].to_i
  end
  single_pairs[a[0].to_sym].store(a[-1].to_sym, score)
end

names.permutation.each do |perm|
  total_hap = perm.map.with_index do |key, index|
    if  index == perm.size - 1
      single_pairs[perm[0]][perm[-1]] + single_pairs[perm[-1]][perm[0]]
    else
      single_pairs[key][perm[index+1]] + single_pairs[perm[index+1]][key]
    end
  end
  answer = total_hap.sum if total_hap.sum > answer
  # p perm, total_hap, answer
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
single_pairs[:me] = names.map { |name| [name, 0]}.to_h
names.each do |name|
  single_pairs[name].store(:me, 0)
end

names += [:me]

names.permutation.each do |perm|
  total_hap = perm.map.with_index do |key, index|
    if  index == perm.size - 1
      single_pairs[perm[0]][perm[-1]] + single_pairs[perm[-1]][perm[0]]
    else
      single_pairs[key][perm[index+1]] + single_pairs[perm[index+1]][key]
    end
  end
  answer = total_hap.sum if total_hap.sum > answer
  # p perm, total_hap, answer
end
puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
