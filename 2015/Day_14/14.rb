class Reindeer
  def initialize(name, speed, move_time, rest_time)
    @name = name
    @speed = speed.to_i
    @move_time = move_time.to_i
    @rest_time = rest_time.to_i
    @cycle_time = move_time.to_i + rest_time.to_i
  end

  attr_accessor :name

  def distance_at(time)
    full_cycle = time / @cycle_time
    mod_of_cycle = time % @cycle_time
    mod_of_move = mod_of_cycle > @move_time ? @move_time : mod_of_cycle
    (full_cycle * @move_time * @speed) + (mod_of_move * @speed)
  end
end

# input for task
inp = IO.read('input.txt')



# part 1
start = Time.now
res = inp.each_line.map do |line|
  arr = line.split
  { name: arr[0],
    speed: arr[3],
    move_time: arr[6],
    rest_time: arr[13] }
end

q = res.map do |attr_hash|
  reindeer = Reindeer.new(attr_hash[:name], attr_hash[:speed], attr_hash[:move_time], attr_hash[:rest_time])
  [reindeer.name, reindeer.distance_at(2503)]
end.to_h

answer = q.values.max #change type if need

#TODO Part 1

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now

reindeers = inp.each_line.map do |line|
  arr = line.split
  Reindeer.new(arr[0], arr[3], arr[6], arr[13])
end
scores = reindeers.map{|key| [key, 0]}.to_h
(1..2503).to_a.map do |sec|
  results_at = reindeers.map { |r| [r.distance_at(sec), r] }
  # p results_at
  max_dist = results_at.max { |a, b| a[0] <=> b[0]}[0]
  results_at.select{|i| i[0] == max_dist}.each do |mid_res|
    scores[mid_res[1]] += 1
  end
  # p sec, scores.map{|k,v| [k.name, v]}.to_h
end
# p scores
answer = scores.values.max #change type if need

#TODO Part 2

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
