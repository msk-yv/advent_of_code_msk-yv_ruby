# class Ingredient
#   # def initialize(name, capacity, durability, flavor, texture, calories)
#   #   # @name = name
#   #   # @capacity = capacity
#   #   # @durability = durability
#   #   # @flavor = flavor
#   #   # @texture = texture
#   #   # @calories = calories
#   # end
#   def capacity; end
#   def durability; end
#   def flavor; end
#   def texture; end
#   def calories; end
# end
#
# class Sugar < Ingredient
#   def capacity
#     0
#   end
#
# end


# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
counter = 0
max_score = 0
(1..97).map do |frosting|
  # p "frosting: #{frosting}"
  (1..(98-frosting)).map do |candy|
    # p "candy: #{candy}"
    (1..(99-frosting-candy)).map do |butterscotch|
      # p "butterscotch: #{butterscotch}"
      (1..(100-frosting-candy-butterscotch)).map do |sugar|
        next if (frosting+candy+butterscotch+sugar) != 100
        next if (frosting * 5 + candy * 8 + butterscotch * 6 + sugar * 1) != 500

        capacity = frosting * 4 + butterscotch * (-1)
        next if capacity <= 0

        durability = frosting * (-2) + candy * 5
        next if durability <= 0

        flavor = candy * (-1) + butterscotch * 5 + sugar * (-2)
        next if flavor <= 0

        texture = sugar * 2
        next if texture <= 0

        counter += 1
        recipe = capacity * durability * flavor * texture
        next if recipe < max_score
        max_score = recipe

        p "#{counter}: recipe=#{recipe} frosting=#{frosting}, candy=#{candy}, butterscotch=#{butterscotch}, sugar=#{sugar}"
      end
    end
  end
end
p counter
answer = max_score #change type if need

#TODO Part 1

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
