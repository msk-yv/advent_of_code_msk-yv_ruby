CLUE_HASH = { children: 3,
              cats: 7,
              samoyeds: 2,
              pomeranians: 3,
              akitas: 0,
              vizslas: 0,
              goldfish: 5,
              trees: 3,
              cars: 2,
              perfumes: 1 }

def checker(attrs)
  bool_arr = CLUE_HASH.keys.map(&:to_s).map do |key|
    res1 = attrs[key]
    res2 = (attrs[key] == CLUE_HASH[key].to_s)

    if res1 && res2
      'true'
    elsif res1 && !res2
      'false'
    else
      'perhaps'
    end
  end
  !bool_arr.any?{ |i| i == 'false' }
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
sues = {}
inp.each_line do |line|
  arr = line.split(':').map { |i| i.split(',') }.flatten.map(&:strip)
  attrs = arr[1..-1].each_slice(2).map{ |i| i }.to_h
  attrs.each_pair do |k, v|
    attrs[k] = v.to_i
  end
  sues[arr[0].split(' ')[-1].to_i] = attrs
end

filtred_sues = sues.select do |sue|
  bool_arr = sues[sue].keys.map do |key|
    sues[sue][key] == CLUE_HASH[key.to_sym]
  end
  bool_arr.all?
end

answer = filtred_sues.first[0]

p "Part 1 answer: #{answer}"
p "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
filtred_sues = sues.select do |sue|
  bool_arr = sues[sue].keys.map do |key|
    case key
    when 'cats', 'trees'
      sues[sue][key] > CLUE_HASH[key.to_sym]
    when 'pomeranians', 'goldfish'
      sues[sue][key] < CLUE_HASH[key.to_sym]
    else
      sues[sue][key] == CLUE_HASH[key.to_sym]
    end
  end
  bool_arr.all?
end
answer = filtred_sues.first[0]
puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
