# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now

answer = []
arr = inp.each_line.map(&:to_i).sort
(4..arr.size).each do |sample_size|
  arr.combination(sample_size).each do |sample|
    if sample.sum == 150
      answer.push(sample)
      # p sample
    end
  end
end
# p answer.uniq.size
#TODO Part 1

puts "Part 1 answer: #{answer.size}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
# answer = 0 #change type if need

#TODO Part 2

puts "Part 2 answer: #{answer.select{ |i| i.size == 4}.size}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
