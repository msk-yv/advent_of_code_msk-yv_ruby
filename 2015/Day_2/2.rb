# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
inp.each_line do |s|
  a = s.split('x').map{ |x| x = x.to_i }.sort
  answer += 3 * a[0] * a[1] + 2 * a[0] * a[2] + 2 * a[1] * a[2]
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
inp.each_line do |s|
  a = s.split('x').map{ |x| x = x.to_i }.sort
  answer += 2 * a[0] + 2 * a[1] + a[0] * a[1] * a[2]
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
