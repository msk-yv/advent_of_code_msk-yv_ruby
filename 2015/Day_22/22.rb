# input for task
# inp=IO.read('input.txt')
# class casts
#     attr_accessor :name, :cost, :damage, :heals, :long, :sheild # аксессоры

#     def initialize (name, age) # конструктор класса.
#         @name = name # переменная объекта
#         @age = age
#     end
# end

player = {
  hit_points: 50,
  armor: 0,
  mana: 500,
  damage: 0
}

boss = {
  hit_points: 51,
  armor: 0,
  mana: 0,
  damage: 9
}

casts = {
  magic_missile: {
    mana_cost: 53,
    damage: 4,
    heal: 0,
    long: 0,
    sheild: 0,
    mana: 0
  },
  drain: {
    mana_cost: 73,
    damage: 2,
    heal: 2,
    long: 0,
    sheild: 0,
    mana: 0
  },
  shield: {
    mana_cost: 113,
    damage: 0,
    heal: 0,
    long: 6,
    sheild: 7,
    mana: 0
  },
  poison: {
    mana_cost: 173,
    damage: 3,
    heal: 0,
    long: 6,
    sheild: 0,
    mana: 0
  },
  recharge: {
    mana_cost: 229,
    damage: 0,
    heal: 0,
    long: 5,
    sheild: 0,
    mana: 101
  }
}

p "player #{player}"
p "boss #{boss}"
p "Player hit_points: #{player[:hit_points]}"
p ''

# part 1
start = Time.now
answer = 0 # change type if need

# TODO: Part 1

p "player #{player}"
p "boss #{boss}"
casts.each do |c|
  c.each { |k, v| p "#{k}: #{v}" }
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 # change type if need

# TODO: Part 2

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
