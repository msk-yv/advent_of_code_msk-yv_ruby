def check_even(x)
  return x != 0 && x % 2 == 0
end

def check_odd(x)
  return x == 1
end

inp = IO.read('input.txt')
instructions = inp.split(10.chr)
a = 1
b = 0
i = 0
while i >= 0 && i < instructions.size
  instruction = instructions[i].split(' ')
  #i += 1
  #puts "#{instruction[0]}, #{instruction[0] == 'jio'}, #{instruction[1][0] == 'a'}, #{instruction[2].to_i}, #{check_even(a)}"
#=begin
  case instruction[0]
  when 'hlf'
    puts 'hlf'
    if instruction[1] == 'a'
      a /= 2
    elsif instruction[1] == 'b'
      b /= 2
    end
    i += 1
  when 'tpl'
    puts 'tpl'
    if instruction[1] == 'a'
      a *= 3
    elsif instruction[1] == 'b'
      b *= 3
    end
    i += 1
  when 'inc'
    puts 'inc'
    if instruction[1] == 'a'
      a += 1
    elsif instruction[1] == 'b'
      b += 1
    end
    i += 1
  when 'jmp'
    i += instruction[1].to_i
  when 'jie'
    if instruction[1][0] == 'a'
      if check_even(a)
        i += instruction[2].to_i
      else
        i += 1
      end
    else
      if check_even(b)
        i += instruction[2].to_i
      else
        i += 1
      end
    end
  when 'jio'
    if instruction[1][0] == 'a'
      if check_odd(a)
        i += instruction[2].to_i
      else
        i+=1
      end
    else
      if check_odd(b)
        i += instruction[2].to_i
      else
        i += 1
      end
    end
  end
#=end
  puts "i=#{i}, a=#{a}, b=#{b}"
end
