# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
arr = []
inp.each_line do |line|
  arr << line.to_i
end

limit = arr.sum / 3

qe = arr[-6..-1].inject(1) { |r, e| r * e }

arr.permutation(6).each do |p|
  if p.sum == limit
    cur_qe = p.inject(1) { |r, e| r * e }
    if cur_qe < qe
      qe = cur_qe
    end
  end
end

answer = qe

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
limit = arr.sum / 4

qe = arr[-4..-1].inject(1) { |r, e| r * e }

arr.permutation(4).each do |p|
  if p.sum == limit
    cur_qe = p.inject(1) { |r, e| r * e }
    if cur_qe < qe
      qe = cur_qe
    end
  end
end

answer = qe

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
