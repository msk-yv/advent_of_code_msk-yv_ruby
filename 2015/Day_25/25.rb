# input for task
inp = 20151125
mult = 252533
devider = 33554393
row = 2947
column = 3029
# To continue, please consult the code grid in the manual.  Enter the code at row 2947, column 3029.

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
i = 1
(1..row-1).each { |j| i += j }
# puts i

(1..column-1).each { |j| i += row + j }

(i-1).times do
    #puts inp
  inp = inp * mult % devider
end

answers= inp

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
