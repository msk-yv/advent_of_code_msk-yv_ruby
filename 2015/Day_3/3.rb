# reqired methods 
def set_address(x, y)
  x.to_s+','+y.to_s
end

def take_dirction(c)
  d = [0, 0]
  case c
  when '<'
    d[0] -= 1
  when '>'
    d[0] += 1
  when '^'
    d[1] += 1
  when 'v'
    d[1] -= 1
  end
  return d
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
x = 0
y = 0
addresList = []
addresList << set_address(x,y)
inp.each_char do |c|
  d = take_dirction(c)
  x += d[0]
  y += d[1]
  currentAddress = set_address(x,y)
  addresList.push(currentAddress) unless addresList.include?(currentAddress)
end
answer = addresList.size

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
santaX = 0
santaY = 0
roboSX = 0
roboSY = 0
addresList = []
addresList << set_address(roboSX, roboSY)
inp.each_char.with_index do |c, i|
  d = take_dirction(c)
  if i % 2 == 0
    santaX += d[0]
    santaY += d[1]
    currentSantaAddress = set_address(santaX,santaY)
    addresList.push(currentSantaAddress) if not addresList.include?(currentSantaAddress)
  else
    roboSX += d[0]
    roboSY += d[1]
    currentSantaAddress = set_address(roboSX,roboSY)
    addresList.push(currentSantaAddress) if not addresList.include?(currentSantaAddress)
  end
end
answer = addresList.size

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"