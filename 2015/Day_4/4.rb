require 'digest'
# input for task
inp='iwrupvqb'

# part 1
start = Time.now
answer = 1 #change type if need

#TODO Part 1
md5 = Digest::MD5.new
md5.update inp
md5 << answer.to_s
while md5.hexdigest[0,5] != '00000'
  answer += 1
  md5.reset
  md5 << inp + answer.to_s
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now

#TODO Part 2

md5 = Digest::MD5.new
md5.update inp
md5 << answer.to_s
while md5.hexdigest[0,6] != '000000'
  answer += 1
  md5.reset
  md5 << inp + answer.to_s
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
