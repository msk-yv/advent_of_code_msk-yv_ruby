# input for task
inp=IO.read('input.txt')

def check_vowels(s)
  vowelsanswer = 0
  ['a','e','i','o','u'].each { |c| vowelsanswer += s.count(c) }
  return vowelsanswer > 2
end

def check_doubles(s)
  'abcdefghijklmnopqrstuvwxyz'.each_char do |c|
    return true if s.include?(c*2)
  end
  return false
end

def check_bad_pairs(s)
  ['ab', 'cd', 'pq', 'xy'].each do |p|
    return false if s.include?(p)
  end
  return true
end

def check_double_pairs(s)
  i = 0
  while i < s.size-1
    pair=s[i,2]
    return true if s[(i+2)..-2].include?(pair)
    i += 1
  end
  return false
end

def check_triplets(s)
  i = 0
  while i < s.size-2
    triplet=s[i,3]
    return true if triplet[0] == triplet[2]
    i += 1
  end
  return false
end

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
inp.each_line do |s|
  if check_doubles(s) && check_vowels(s) && check_bad_pairs(s)
    answer += 1
  end
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2

inp.each_line do |s|
  if check_triplets(s) && check_double_pairs(s) #&& check_vowels(s) && check_bad_pairs(s)
    answer += 1
  end
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"