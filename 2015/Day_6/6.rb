def toggle(start_i, start_j, finish_i, finish_j)
  (start_i..finish_i).each do |i|
    (start_j..finish_j).each do |j|
      $a[i][j] = $a[i][j] == 0 ? 1 : 0
    end
  end
end

def switch(start_i, start_j, finish_i, finish_j, s)
  (start_i..finish_i).each do |i|
    (start_j..finish_j).each do |j|
      $a[i][j] = s
    end
  end
end

def toggle_p2(start_i, start_j, finish_i, finish_j)
    (start_i..finish_i).each do |i|
      (start_j..finish_j).each do |j|
        $b[i][j] += 2
      end
    end
  end
  
  def switch_p2(start_i, start_j, finish_i, finish_j, s)
    (start_i..finish_i).each do |i|
      (start_j..finish_j).each do |j|
        $b[i][j] = $b[i][j] + s < 0 ? 0 : $b[i][j] + s 
      end
    end
  end

# input for task
inp = IO.read('input.txt')

# part 1
start = Time.now
answer = 0 

#TODO Part 1
$a = Array.new(1000).map!{Array.new(1000)}
$a.each_index do |i|
  $a[i].each_index { |j| $a[i][j] = 0 }
end

inp.each_line do |line|
  start_i = line.split(' through ')[0].split(',')[0].split(' ')[-1].to_i
  start_j = line.split(' through ')[0].split(',')[1].to_i
  finish_i = line.split(' through ')[1].split(',')[0].to_i
  finish_j = line.split(' through ')[1].split(',')[1].to_i
  helper = line.split(' ')
  if helper[0] == 'toggle'
    toggle(start_i, start_j, finish_i, finish_j)
  elsif helper[1] == 'on'
    switch(start_i, start_j, finish_i, finish_j, 1)
  else
    switch(start_i, start_j, finish_i, finish_j, 0)
  end 
end

$a.each do |line|
  answer += line.sum
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2
$b = Array.new(1000).map!{Array.new(1000)}
$b.each_index do |i|
  $b[i].each_index { |j| $b[i][j] = 0 }
end

inp.each_line do |line|
  start_i = line.split(' through ')[0].split(',')[0].split(' ')[-1].to_i
  start_j = line.split(' through ')[0].split(',')[1].to_i
  finish_i = line.split(' through ')[1].split(',')[0].to_i
  finish_j = line.split(' through ')[1].split(',')[1].to_i
  helper = line.split(' ')
  if helper[0] == 'toggle'
    toggle_p2(start_i, start_j, finish_i, finish_j)
  elsif helper[1] == 'on'
    switch_p2(start_i, start_j, finish_i, finish_j, 1)
  else
    switch_p2(start_i, start_j, finish_i, finish_j, -1)
  end 
end

$b.each do |line|
  answer += line.sum
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"