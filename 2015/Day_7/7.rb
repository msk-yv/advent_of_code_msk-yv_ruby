class String
  def is_i?
    /\A[-+]?\d+\z/ === self
  end
end

def evaluate(s)
  if !$values[s].nil?
    $values[s]
  elsif s.is_i?
    s.to_i
  else
    a = $circuit[s].split(' ')
    if a.size == 1
      $values[s] = evaluate(a[0])
    elsif a[0] == 'NOT'
      $values[s] = 2**16 + ~(evaluate(a[1]))
    else
      case a[1]
      when 'RSHIFT'
        $values[s] = evaluate(a[0]).to_i >> a[2].to_i
      when 'LSHIFT'
        $values[s] = evaluate(a[0]).to_i << a[2].to_i
      when 'OR'
        $values[s] = evaluate(a[0]).to_i | evaluate(a[2])
      when 'AND'
        $values[s] = evaluate(a[0]).to_i & evaluate(a[2])
      end
    end
  end
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
$circuit = {}
$values = {}
inp.each_line do |line|
  target = line.split(' -> ')[1][0..-2]
  source = line.split(' -> ')[0]
  if source.is_i?
    $values[target] = source
  end
  $circuit[target] = source
end

answer = evaluate('a')

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
# answer = 0 #change type if need

#TODO Part 2
$values = {}
$values['b'] = answer
$values['c'] = 0 
answer = evaluate('a')

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
