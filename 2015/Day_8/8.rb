# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

#TODO Part 1
inp.each_line do |line|
  puts line  
  code_chars = line.size - 1 # delete \n symbol in end of each line
  sym_chars = 0#line[1..-3].size - line.count("\\") - line.split('\\').select{ |x| x[0] == 'x'}.count * 3
  #-  -line.split('\\').select{ |x| x[0] == 'x'}.count * 2 
#   # line.size - 3 #  #, line.count('\\'), line.count('\\x')
#   line[1..-3].each_char.with_index do |c, i|
#     sym_chars += 1 if c != "//"
#     puts "#{i}: #{c}"
#   end
  i = 1
  while i < line.size - 2
    if line[i] != "\\"
      sym_chars += 1
      i += 1
    elsif line[i + 1] == 'x'
      sym_chars += 1
      i += 4
    else
      sym_chars += 1
      i += 2
    end
  end

  answer += code_chars - sym_chars
  puts line[1..-3].size, sym_chars
end


puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"