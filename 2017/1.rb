arr = IO.read("input1.txt").split('').map { |x| x.to_i }
sum = 0 
arr.each_with_index do |x,i|
	sum += x if i < arr.size - 1 && arr[i.next] == x
end
sum += arr[-1] if arr[0] = arr[-1]
puts sum
# second part
# arr = [1, 2, 1, 2]
off = arr.size / 2
sum = 0 
arr.each_with_index do |x,i|
	next_i = (i + off) % arr.size
	# puts i, next_i
	sum += x if arr[next_i] == x
end
# sum += arr[-1] if arr[0] = arr[-1]
puts "Second part: #{sum}"