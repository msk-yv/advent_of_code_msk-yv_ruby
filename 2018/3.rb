field = []
(0..1001).each do |i|
  field.push([])
  (0..1001).each do |j|
    field[i].push(0)
  end
end
#puts field
inp=IO.read('input3.txt')
counter = 0
inp.split(10.chr).each do |s|
  n = s.split(' @ ')[0].split('#')[1].to_i
  r = s.split(' @ ')[1].split('#')[0].to_i
  c = s.split(' @ ')[1].split(',')[1].split(':')[0].to_i
  w = s.split(' @ ')[1].split(',')[1].split(': ')[1].split('x')[0].to_i
  t = s.split(' @ ')[1].split(',')[1].split(': ')[1].split('x')[1].to_i
  #puts s, 'n=' + n.to_s + ';r=' + r.to_s + ';c=' + c.to_s + ';w=' + w.to_s + ';t=' + t.to_s + ';   counter=' + counter.to_s
  (r..r+w-1).each do |i|
    (c..c+t-1).each do |j|
      if field[i][j] == 0
        field[i][j] = n
        #puts 'i=' + i.to_s + '; j=' + j.to_s, field[i][j]
      elsif field[i][j] != -1
        field[i][j] = -1
        counter +=1
      end
    end
  end
end
a = Array.new(1025){ |i| i }
puts a
inp.split(10.chr).each do |s|
  n = s.split(' @ ')[0].split('#')[1].to_i
  r = s.split(' @ ')[1].split('#')[0].to_i
  c = s.split(' @ ')[1].split(',')[1].split(':')[0].to_i
  w = s.split(' @ ')[1].split(',')[1].split(': ')[1].split('x')[0].to_i
  t = s.split(' @ ')[1].split(',')[1].split(': ')[1].split('x')[1].to_i
  flag = true
  (r..r+w-1).each do |i|
    (c..c+t-1).each do |j|
      if field[i][j] != n
        a.delete(n)
        break
      end
    end
  end
end
puts a
puts counter
