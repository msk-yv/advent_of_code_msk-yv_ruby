def reduceStr(inp)
  l = 'abcdefghijklmnopqrstuvwxyz'
  i = 0
  s = inp
  while i < (s.size - 1) do
    if l.include? s[i]
      if s[i+1].ord == s[i].ord - 32
        #puts s[i-5,5] + ' ' + s[i+2,5]
        s = s[0,i] + s[i+2..-1]
        #puts 'i=' + i.to_s + '; size=' + s.size.to_s
        if i > 0
          i -= 1
        end
        next
      end
    else
      if s[i+1].ord == s[i].ord + 32
        #puts s[i-5,5] + ' ' + s[i+2,5]
        s = s[0,i] + s[i+2..-1]
        #puts 'i=' + i.to_s + '; size=' + s.size.to_s
        if i > 0
          i -= 1
        end
        next
      end
    end
    i += 1
  end
  return s
end

inp=IO.read('input5.txt')
l = 'abcdefghijklmnopqrstuvwxyz'
m = inp.size
l.chars do |ch|
  x = inp
  x = x.split(ch).join('')
  x = x.split(ch.upcase).join('')
  a = reduceStr(x).size
  if a < m 
    m = a
  end
  puts ch, m
end

