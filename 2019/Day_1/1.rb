def recursive_fuel_counter(i)
  fuel_to_add = (i / 3) - 2
  unless fuel_to_add < 9
    fuel_to_add = fuel_to_add + recursive_fuel_counter(fuel_to_add)
  end
  fuel_to_add
end

# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now

answer = inp.split("\n").map(&:to_i).map{ |i| (i / 3) - 2 }.sum

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

#TODO Part 2

answer = inp.split("\n").map(&:to_i).map{ |i| recursive_fuel_counter(i) }.sum
puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"

