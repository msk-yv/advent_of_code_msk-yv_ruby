# input for task
inp=IO.read('input.txt')


arr = inp.each_line.map do |str|
  str.to_i
end

# part 1
start = Time.now
answer = 0 #change type if need

arr.sort!.reverse.each do |first_num|
  (arr - [first_num]).each do |second_num|
    next if first_num + second_num != 2020
    answer = first_num * second_num
  end
end


puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now

arr.sort!.reverse.each do |first_num|
  (arr - [first_num]).each do |second_num|
    (arr - [first_num, second_num]).each do |third_num|
      next if first_num + second_num + third_num != 2020
      answer = first_num * second_num * third_num
    end
  end
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
