# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0
inp.each_line do |line|

  min_count = line.split('-').first.to_i
  max_count = line.split('-').last.split(' ').first.to_i
  char_to_check = line.split(':').first.split(' ').last
  char_count = line.gsub("\n", '').split(':').last.count(char_to_check)
  flag = (char_count >= min_count && char_count <= max_count)
  if flag
    answer += 1
  end
end
puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

inp.each_line do |line|
  min_count = line.split('-').first.to_i
  max_count = line.split('-').last.split(' ').first.to_i
  char_to_check = line.split(':').first.split(' ').last
  pass = line.gsub("\n", '').split(':').last
  flag = ((pass[min_count] == char_to_check) ^ (pass[max_count] == char_to_check))
  # p line, pass.split(''), pass[min_count], pass[max_count], flag unless flag
  if flag
    answer += 1
  end
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
