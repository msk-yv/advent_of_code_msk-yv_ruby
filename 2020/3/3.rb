# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

step = 0
inp.each_line do |line|
  str = line.gsub("\n", '')

  if line[step] == '#'
    answer += 1
  end
  step = (step + 3) % 31
end

#TODO Part 1

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now

slopes = [0, answer, 0, 0, 0]

step = 0
inp.each_line do |line|
  slopes[0] += 1 if line[step] == '#'
  step = (step + 1) % 31
end

step = 0
inp.each_line do |line|
  slopes[2] += 1 if line[step] == '#'
  step = (step + 5) % 31
end

step = 0
inp.each_line do |line|
  slopes[3] += 1 if line[step] == '#'
  step = (step + 7) % 31
end

step = 0
inp.each_line.with_index do |line, index|
  next if index.odd?
  slopes[4] += 1 if line[step] == '#'
  step = (step + 1) % 31
end

puts "Part 2 answer: #{slopes.inject(:*)}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
