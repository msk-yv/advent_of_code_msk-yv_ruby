require 'byebug'

REQUIRE_KEYS = %w[byr iyr eyr hgt hcl ecl pid].sort.freeze
BIRTH_RANGE = (1920..2002).to_a.freeze
ISSUE_RANGE = (2010..2020).to_a.freeze
EXPIRE_RANGE = (2020..2030).to_a.freeze
CM_HEIGHT_RANGE = (150..193).to_a.freeze
IN_HEIGHT_RANGE = (59..76).to_a.freeze
EYES_COLORS = %w[amb blu brn gry grn hzl oth].freeze
# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

indicator = 0
arr = inp.split("\n")
while  indicator < arr.size
  index = arr[indicator..-1].index('')
  # break unless index
  passport_pretend = if index
                       arr[indicator..(index - 1)]
                     else
                       arr
                     end.join(' ').split(' ').map { |item| item.split(':').first }
  flag = passport_pretend && (passport_pretend - %w[cid]).sort == REQUIRE_KEYS
  # p passport_pretend, (passport_pretend - %w[cid]).sort, REQUIRE_KEYS, flag
  answer +=1 if flag
  arr = if index
          arr[(index + 1)..-1]
        else
          []
        end
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
def check_values(with_values_pretend)
  hash_to_check = with_values_pretend.join(' ').split(' ').map { |i| i.split(':') }.to_h
  checks_arr = hash_to_check.map do |k, v|
    case k
    when 'byr'
      v.match(/^\d{4}$/) && BIRTH_RANGE.include?(v.to_i)
    when 'iyr'
      v.match(/^\d{4}$/) && ISSUE_RANGE.include?(v.to_i)
    when 'eyr'
      v.match(/^\d{4}$/) && EXPIRE_RANGE.include?(v.to_i)
    when 'hgt'
      if v.include?('cm')
        CM_HEIGHT_RANGE.include?(v.split('cm').first.to_i)
      elsif v.include?('in')
        IN_HEIGHT_RANGE.include?(v.split('in').first.to_i)
      else
        false
      end
    when 'hcl'
      v[0] == '#' && v.size == 7 && v[1..-1].split('').map{ |i| 'abcdef0123456789'.include?(i) }.all?
    when 'ecl'
      EYES_COLORS.include?(v)
    when 'pid'
      if v.match(/^\d{9}$/)
        true
      else
        false
      end
    when 'cid'
      true
    end
  end
  checks_arr.all?
end
start = Time.now
answer = 0 #change type if need

indicator = 0
arr = inp.split("\n")
while  indicator < arr.size
  index = arr[indicator..-1].index('')

  with_values_pretend = if index
                          arr[indicator..(index - 1)]
                        else
                          arr
                        end
  passport_pretend = with_values_pretend.join(' ').split(' ').map { |item| item.split(':').first }
  flag = passport_pretend && (passport_pretend - %w[cid]).sort == REQUIRE_KEYS

  answer +=1 if flag && check_values(with_values_pretend)
  arr = if index
          arr[(index + 1)..-1]
        else
          []
        end
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
