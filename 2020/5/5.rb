def divider_8bit(start, finish, str)
  if str.size == 1
    if str == 'F'
      start
    else
      finish
    end
  else
    new_start, new_finish = if str[0] == 'F'
                              [start, (finish + start) / 2]
                            else
                              [((finish + start) / 2) + 1, finish]
                            end
    divider_8bit(new_start, new_finish, str[1..-1])
  end
end

def count_row(str)
  start = 0
  finish = 127
  divider_8bit(start, finish, str)
end

def count_column(str)
  start = 0
  finish = 7
  new_str = str.gsub('L', 'F').gsub('R', 'B')
  divider_8bit(start, finish, new_str)
end

# input for task
inp=IO.read('input.txt').each_line.map { |line| line.gsub("\n", '')}

# part 1
start = Time.now

indicators = inp.map do |talon|
  count_row(talon[0..6]) * 8 + count_column(talon[7..9])
end

answer = indicators.max
puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
range = (indicators.min..indicators.max).to_a
answer = range - indicators

#TODO Part 2

puts "Part 2 answer: #{answer.first}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
