# input for task
inp=IO.read('input.txt')

# part 1
start = Time.now
answer = 0 #change type if need

indicator = 0
arr = inp.split("\n")
while  indicator < arr.size
  index = arr[indicator..-1].index('')

  group_answers = if index
                    arr[indicator..(index - 1)]
                  else
                    arr
                  end
  answer += group_answers.join('').split('').uniq.size

  arr = if index
          arr[(index + 1)..-1]
        else
          []
        end
end

puts "Part 1 answer: #{answer}"
puts "Time to Part 1 answer: #{Time.now - start} sec"

# part 2
start = Time.now
answer = 0 #change type if need

indicator = 0
arr = inp.split("\n")
while  indicator < arr.size
  index = arr[indicator..-1].index('')

  group_answers = if index
                    arr[indicator..(index - 1)]
                  else
                    arr
                  end

  answer += group_answers.map { |i| i.split('') }.inject { |memo, i| memo & i }.size

  arr = if index
          arr[(index + 1)..-1]
        else
          []
        end
end

puts "Part 2 answer: #{answer}"
puts "Time to Part 2 answer: #{Time.now - start} sec"
